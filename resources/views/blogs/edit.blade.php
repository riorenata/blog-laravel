@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">Edit Blog</div>

        <div class="card-body">

          <form action="{{ route('blogs.update', $blog->id) }}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
              <label for="title">Title</label>
              <input type="text" name="title" id="title" class="form-control" value="{{ old('title', $blog['title']) }}">
            </div>
            <div class="form-group">
              <label for="slug">Slug</label>
              <input type="text" name="slug" id="slug" class="form-control" value="{{ old('slug', $blog['slug']) }}">
            </div>
            <div class="form-group">
              <label for="content">Content</label>
              <textarea name="content" id="content" cols="30" rows="10" class="form-control">{{ old('content', $blog['content']) }}</textarea>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-outline-primary form-control mt-4">
                Edit
              </button>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
</div>
@endsection