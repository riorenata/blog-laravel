@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">Create Blog</div>

        <div class="card-body">

          <form action="{{ route('blogs.store') }}" method="POST">
            @csrf
            <div class="form-group">
              <label for="title">Title</label>
              <input type="text" name="title" id="title" class="form-control" value="{{ old('title') }}" required>
            </div>
            <div class="form-group">
              <label for="content">Content</label>
              <textarea name="content" id="content" cols="30" rows="10" class="form-control" required>{{ old('content') }}</textarea>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-outline-primary form-control mt-4">
                Create
              </button>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
</div>
@endsection