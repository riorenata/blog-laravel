@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">{{ __('Dashboard') }}</div>

        <div class="card-body">
          <!-- @if (session('status'))
          <div class="alert alert-success" role="alert">
            {{ session('status') }}
          </div>
          @endif -->

          <!-- {{ __('You are logged in!') }} -->

          <table class="table">
            <thead>
              <tr>
                <th>
                  Id
                </th>
                <th>
                  Name
                </th>
                <th>
                  Email
                </th>
                <th>
                  Created At
                </th>
                <th>
                  Updated At
                </th>
              </tr>
            </thead>
            <tbody>
              @foreach ($users as $user)
              <tr>
                <td>{{ $user->id }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->created_at }}</td>
                <td>{{ $user->updated_at }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>

          <table class="mt-4 table">
            <thead>
              <tr>
                <th>
                  Id
                </th>
                <th>
                  User Id
                </th>
                <th>
                  Title
                </th>
                <th>
                  Slug
                </th>
                <th>
                  Content
                </th>
                <th>
                  Created At
                </th>
                <th>
                  Updated At
                </th>
              </tr>
            </thead>
            <tbody>
              @foreach ($blogs as $blog)
              <tr>
                <td>{{ $blog->id }}</td>
                <td>{{ $blog->user_id }}</td>
                <td>{{ $blog->title }}</td>
                <td>{{ $blog->slug }}</td>
                <td>{{ $blog->content }}</td>
                <td>{{ $blog->created_at }}</td>
                <td>{{ $blog->updated_at }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>

        </div>
      </div>
    </div>
  </div>
</div>
@endsection