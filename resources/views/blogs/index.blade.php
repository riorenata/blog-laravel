@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">

    <div class="mb-4">
      <a href="{{ route('blogs.create') }}" class="btn btn-success">Create</a>
    </div>

    @foreach ($blogs as $blog)
    <div class="col-0 col-md-4 mb-4">
      <div class="card shadow">
        <div class="card-header text-center">{{ $blog['title'] }}</div>
        <div class="card-body">
          {{ substr($blog['content'],0,200) }}...
        </div>
        <div class="card-footer">
          <form action="{{ route('blogs.destroy', $blog['id']) }}" method="POST">
            <div class="form-group">
              <a href="{{ route('blogs.show', $blog['id']) }}" class="btn btn-info">Read More</a>
              <a href="{{ route('blogs.edit', $blog['id']) }}" class="btn btn-warning">Edit</a>
              @csrf
              @method('DELETE')
              <button type="submit" class="btn btn-danger">Delete</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    @endforeach
  </div>
</div>
@endsection