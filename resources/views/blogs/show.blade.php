@extends('layouts.app')

@section('content')
<div class="container">

  <article>
    <h1 class="text-center">{{ $blog['title'] }}</h1>
    <span>Author: <i>{{ $author }}</i></span>
    <hr>
    <p>
      <span>{{ $blog['content'] }}</span>
    </p>
    <div class="position-absolute bottom-0 mb-4">
      <a href="{{ route('blogs.index') }}" class="btn btn-outline-primary"><span>
          <- Back</span></a>
    </div>
  </article>

</div>
@endsection