<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\User;
use Illuminate\Http\Request;

class BlogController extends Controller
{

    /**
     * Authentication method
     * 
     * @return \Illuminate\Routing\Controller::middleware
     */
    public function __construct()
    {
        return $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::all();
        return view('blogs.index', [
            'blogs' => $blogs
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blogs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'title' => 'required|max:255',
            'content' => 'required'
        ]);
        if (!$validate) return redirect()->route('blogs.create');
        date_default_timezone_set('UTC');
        $datetime = date("Y-m-d H:i:s"); //today();
        $blogs = [
            'title' => $request->title,
            'slug' => str_replace(' ', '-', strtolower($request->title)),
            'content' => $request->content,
            'created_at' => $datetime,
            'updated_at' => $datetime,
            'user_id' => $request->user()->id,
        ];
        // $blogs['slug'] = str_replace(' ', '-', strtolower($blogs['title']));
        // dd($blogs['slug']);
        Blog::create($blogs);
        return redirect()->route('blogs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blog = Blog::find($id);
        // dd(User::firstWhere('id', $blog->user_id)->name);
        $user = $blog->user;
        return view('blogs.show', [
            'blog' => $blog,
            'author' => $user->name
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::find($id);
        return view('blogs.edit', [
            'blog' => $blog
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $request->validate([
            'title' => 'required|max:255',
            'content' => 'required'
        ]);
        if (!$validate) return redirect()->route('blogs.edit');
        date_default_timezone_set('UTC');
        $datetime = date("Y-m-d H:i:s");
        $blogs = [
            'title' => $request->title,
            'slug' => $request->slug,
            'content' => $request->content,
            'updated_at' => $datetime,
        ];
        $blog = Blog::find($id);
        $blog->update($blogs);
        return redirect()->route('blogs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = Blog::find($id);
        $blog->delete();
        return redirect()->route('blogs.index');
    }
}
